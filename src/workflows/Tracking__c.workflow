<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>Photo uploaded</fullName>
        <actions>
            <name>PHOTO_UPLOADED</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Tracking__c.CreatedDate</field>
            <operation>greaterThan</operation>
            <value>10/1/2016</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>PHOTO_UPLOADED</fullName>
        <assignedTo>simas@123c.demo.org</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Tracking__c.CreatedDate</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>PHOTO UPLOADED</subject>
    </tasks>
</Workflow>
