<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Information on customer&apos;s (Contact or Account in  SFDC) bank account</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Bank_account_ID__c</fullName>
        <description>As per Fido
id integer 
Unique identifier of the object</description>
        <externalId>true</externalId>
        <label>Bank account ID</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Cash_flow_per_year__c</fullName>
        <description>As per Fido:
cash_flow_per_year number 
Amount available for yearly cash flow. This is the limit of funds an account holder has at their disposal without fulfilling Germany KYC requirements.</description>
        <externalId>false</externalId>
        <label>Cash flow per year</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Currency__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>currency string 3
Account currency. ISO 4217 alpha-3 - 3 letter upcase e.g EUR</description>
        <externalId>false</externalId>
        <label>Currency</label>
        <length>3</length>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>IBAN__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>iban string ≤ 34
IBAN</description>
        <externalId>false</externalId>
        <label>IBAN</label>
        <length>34</length>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Is_locked__c</fullName>
        <defaultValue>false</defaultValue>
        <description>is_locked boolean 
Indicates whether the account is locked</description>
        <externalId>false</externalId>
        <label>Locked</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Is_trusted__c</fullName>
        <defaultValue>false</defaultValue>
        <description>is_trusted boolean 
Indicates if this is an escrow account</description>
        <externalId>false</externalId>
        <label>Escrow account</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Overdraft__c</fullName>
        <description>overdraft number 
Available account overdraft</description>
        <externalId>false</externalId>
        <label>Overdraft</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Owner__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>customers array 
The owners of the account</description>
        <externalId>false</externalId>
        <label>Owner</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>BankAccounts</relationshipLabel>
        <relationshipName>BankAccounts</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Preauth_amount__c</fullName>
        <description>preauth_amount number 
Amount available for pre-authorization.</description>
        <externalId>false</externalId>
        <label>Amount available</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Updated_at__c</fullName>
        <description>updated_at string (date-time) 
Last update date-time.</description>
        <externalId>false</externalId>
        <label>Last update</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>available_balance__c</fullName>
        <description>balance_available number 
Available account balance.</description>
        <externalId>false</externalId>
        <label>Available balance</label>
        <precision>18</precision>
        <required>true</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>balance__c</fullName>
        <description>balance number 
Account balance</description>
        <externalId>false</externalId>
        <label>Balance</label>
        <precision>18</precision>
        <required>true</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>bank_account_number__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <inlineHelpText>As per Fidor:
account_number string ≤ 10
The bank account number.</inlineHelpText>
        <label>Bank account number</label>
        <length>10</length>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>created_at__c</fullName>
        <description>created_at string (date-time) 
Creation date-time, never changes.</description>
        <externalId>false</externalId>
        <label>Created at</label>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>is_debit_note_enabled__c</fullName>
        <defaultValue>false</defaultValue>
        <description>is_debit_note_enabled boolean 
Whether this account is authorized to initiate direct debit transactions.</description>
        <externalId>false</externalId>
        <label>Direct debit enabled</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>BankAccount</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>BankAccount No</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>BankAccounts</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
