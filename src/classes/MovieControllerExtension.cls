public with sharing class MovieControllerExtension {
 
    private ApexPages.standardController controller;
     
    private Photo__c movie;
 
    private List<Id> photoIds;
 
    public MovieControllerExtension(ApexPages.StandardController controller) {
        this.controller = controller;
         
        this.movie = (Photo__c)controller.getRecord();
    }
 
    public List<Id> photos {
        get {
            if(photoIds == null) {
                photoIds = new List<Id>();
                for(Attachment att : [select Id from Attachment where ParentId = :movie.Id]) {
                    photoIds.Add(att.Id);
                }
            }
                             
            return photoIds;
        }
    }
     
    public Integer totalPhotos {
        get {
            return photos.size();
        }
    }
}