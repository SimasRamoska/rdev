public with sharing class PhotoAndTotals {
   	public Photo__c cs {get; set;}
   	public Integer locationTotal {get; set;}
   	public Integer originTotal {get; set;}

	public PhotoAndTotals(Photo__c inCs, Integer inLocationTotal, Integer inOriginTotal)
	{
		cs=inCs;
		locationTotal=inLocationTotal;
		originTotal=inOriginTotal;
	}    	
}