public with sharing class PhotoControllerExtension {
 
    private ApexPages.standardController controller;
     
    private List<Photo__c> photoRecords; 
	String queryStr='select id from Photo__c ';  //Formuojam uzklausos eilute
	private List<Id> photoIds;
 
    public PhotoControllerExtension(ApexPages.StandardController controller) {
        this.controller = controller;
         
       // this.movie = (Photo__c)controller.getRecord();
        this.photoRecords = Database.query(queryStr); //Surenkame Photo__c irasu Id
         System.debug('Cia photoRecords size ' + photoRecords.size());
    }

    public List<Id> photos {
        
        get {
            if(photoIds == null) {
                photoIds = new List<Id>();
                For (Integer I = 0; I < photoRecords.size(); I++){
                    for(Attachment att : [select Id from Attachment where ParentId = :photoRecords[I].Id]) {
                    photoIds.Add(att.Id);
                    }
                }
            }
                             
            return photoIds;
        }
    }
     
    public Integer totalPhotos {
        get {
            return photos.size();
        }
    }
}