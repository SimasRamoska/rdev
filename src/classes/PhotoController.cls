public with sharing class PhotoController {

   
    public boolean displaying { get; set; }
    public Photo__c pageContact; //ok
    public Blob profilePicFile { get; set; }
    public Id currentPicture { get; set; } //ok
    
    /** Constructor, grab record, and check/load an existing photo */
    public PhotoController(ApexPages.StandardController controller) {
        pageContact = (Photo__c)controller.getRecord();
        
        List<attachment> currentPictures = [SELECT Id FROM Attachment WHERE parentId = :pageContact.Id LIMIT 1];
        if(currentPictures.size() != 0) {
            currentPicture = currentPictures.get(0).Id;
        }
        
        displaying = true;
    }

}